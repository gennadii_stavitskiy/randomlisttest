# README #

Coding task:
“Write C# code to implement an efficient algorithm to create a 52 element array containing the numbers 1 to 52, randomly ordered. Each number should only occur once in the resultant array. 
 
Assume that you have access to a function called Random() that generates a random number in a specified range. This function will accept two parameters. The first is the LowNumber of the range and the second is the HighNumber of the range. The function will return a value that is >= LowNumber and <= HighNumber.” 