using ConsoleApp;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;

namespace RandomListTests.RandomStrategies
{
    [TestClass]
    public class TwoArrayRandomTests
    {          
        [TestMethod]
        public void TwoArrayRandom_Can_Create()
        {
            var arrayRandom = new TwoArrayRandomService();

            Assert.IsNotNull(arrayRandom);
        }

        [TestMethod]
        public void TwoArrayRandom_Can_Perform()
        {
            var arrayRandom = new TwoArrayRandomService();

            arrayRandom.Run(52);

            Assert.IsTrue(arrayRandom.ResultArray.Length == 52);
        }

        [TestMethod]
        public void TwoArrayRandom_Perform_Fill_All()
        {
            var arrayRandom = new TwoArrayRandomService();

            arrayRandom.Run(52);

            Assert.IsTrue(arrayRandom.ResultArray.Length == 52);

            for (int i = 1; i <= 52; i++)
            {
                Assert.IsTrue(arrayRandom.ResultArray.Any(r => r == i));
            }            
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TwoArrayRandom_Perform_ThrowsArgumentException_When_Length_Zero()
        {
            var arrayRandom = new TwoArrayRandomService();

            arrayRandom.Run(0);            
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TwoArrayRandom_Perform_ThrowsArgumentException_When_Length_LessZero()
        {
            var arrayRandom = new TwoArrayRandomService();

            arrayRandom.Run(-1);
        }
    }
}
