﻿using ConsoleApp;
using System;

namespace RandomList.RandomStrategies
{
    public class ServiceFactory : IServiceFactory
    {
        public IRandomListService Get(RandomListStrategyItems item)
        {
            switch (item)
            {
                case RandomListStrategyItems.TwoArrayRandom:
                    return new TwoArrayRandomService();
                case RandomListStrategyItems.HashSetRandom:
                    return new HashSetRandomService();
                case RandomListStrategyItems.LinqArrayRandom:
                    return new HashSetRandomService();
                default:
                    throw new NotImplementedException($"RandomListStrategyItems not implemented {item.ToString()}");
            }
        }
    }
}
