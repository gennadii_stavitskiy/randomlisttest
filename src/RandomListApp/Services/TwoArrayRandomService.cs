﻿using ConsoleApp.Util;
using System;

namespace ConsoleApp
{
    /// <summary>
    /// TwoArrayRandom
    /// </summary>
    /// <seealso cref="ConsoleApp.IRandomListService" />
    public class TwoArrayRandomService : IRandomListService
    {
        Random rnd = new Random();

        /// <summary>
        /// Performs the specified random fill logic.
        /// </summary>
        /// <param name="length">The length.</param>
        public void Run(int length)
        {
            Guard.IntMoreThanZero(length, nameof(length));

            this.ResultArray = new int[length];

            int[] array = new int[length];
            
            int index = 0;

            while (index < length)
            {
                var value = rnd.Next(1, length + 1);

                if (array[value - 1] == 0)
                {
                    array[value - 1] = value;
                    ResultArray[index] = value;
                    index++;
                }                
            }
        }

        /// <summary>
        /// Gets the result array.
        /// </summary>       
        public int[] ResultArray { get; private set; } 
    }
}
