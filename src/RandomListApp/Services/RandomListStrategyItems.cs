﻿namespace RandomList.RandomStrategies
{
    public enum RandomListStrategyItems
    {
        TwoArrayRandom,
        HashSetRandom,
        LinqArrayRandom
    }
}
