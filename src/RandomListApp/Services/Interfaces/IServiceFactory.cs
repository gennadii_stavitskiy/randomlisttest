﻿using ConsoleApp;

namespace RandomList.RandomStrategies
{
    public interface IServiceFactory
    {
        IRandomListService Get(RandomListStrategyItems item);
    }
}