﻿namespace ConsoleApp
{
    public interface IRandomListService
    {
        int[] ResultArray { get; }

        void Run(int length);
    }
}