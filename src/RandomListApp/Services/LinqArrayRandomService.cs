﻿using System;
using System.Linq;

namespace ConsoleApp
{
    class LinqArrayRandomService: IRandomListService
    {
        Random rnd = new Random();        

        public void Run(int length)
        {
            this.ResultArray = Enumerable.Range(1, length).OrderBy(x => rnd.Next()).ToArray();
        }

        public int[] ResultArray { get; private set; }
    }
}
