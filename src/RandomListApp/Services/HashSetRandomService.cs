﻿using System;
using System.Collections.Generic;

namespace ConsoleApp
{
    public class HashSetRandomService : IRandomListService
    {
        Random rnd = new Random();

        public void Run(int length)
        {
            this.ResultArray = new int[length];

            var hashSet = new HashSet<int>();            
            
            int index = 0;

            while (index < length)
            {
                var value = rnd.Next(1, length + 1);

                if(!hashSet.Contains(value))                
                {
                    hashSet.Add(value);                    
                    ResultArray[index] = value;
                    index++;
                }                
            }
        }

        public int[] ResultArray { get; private set; } 
    }
}
