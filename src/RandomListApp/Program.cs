﻿using Microsoft.Extensions.DependencyInjection;
using RandomList.RandomStrategies;
using System;
using System.Diagnostics;
using System.Linq;

namespace ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Configure(out ServiceProvider serviceProvider);

            var factory = serviceProvider.GetService<IServiceFactory>();

            PerformanceTest(factory.Get(RandomListStrategyItems.TwoArrayRandom), "TwoArrayRandom");
            
            PerformanceTest(factory.Get(RandomListStrategyItems.HashSetRandom), "HashSetRandom");

            PerformanceTest(factory.Get(RandomListStrategyItems.LinqArrayRandom), "LinqArrayRandom");

            Console.WriteLine("Press any key...");
            Console.ReadKey();
        }

        static void PerformanceTest(IRandomListService randomStrategy, string name)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            randomStrategy.Run(52);
            sw.Stop();

            Console.WriteLine($"{name}");
            Console.WriteLine(string.Join(",", randomStrategy.ResultArray.ToList().Select(r => r.ToString())));
            Console.WriteLine($"Time:{sw.Elapsed}");
        }

        private static void Configure(out ServiceProvider serviceProvider)
        {
            //setup DI
            serviceProvider = new ServiceCollection()                
                .AddTransient<IServiceFactory, ServiceFactory>()                
                .BuildServiceProvider();            
        }
    }
}
